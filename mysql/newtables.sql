CREATE TABLE Google API User (
  GeolocationAPI User INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT  ,
PRIMARY KEY(GeolocationAPI User))
TYPE=InnoDB;


CREATE TABLE Users (
  MemberID INTEGER UNSIGNED ZEROFILL  NOT NULL DEFAULT 8  AUTO_INCREMENT,
  NameFL VARCHAR BINARY  NULL DEFAULT 50 ,
  EmailAdress VARCHAR BINARY  NULL DEFAULT 50 ,
  password_hash VARCHAR(45) BINARY  NULL DEFAULT 45 ,
  api_key VARCHAR BINARY  NULL DEFAULT 150 ,
  status_2 INTEGER UNSIGNED ZEROFILL  NULL DEFAULT 5 ,
  Created_at TIME  NULL    ,
PRIMARY KEY(MemberID));


CREATE TABLE Calories (
  Calories INTEGER UNSIGNED  NOT NULL DEFAULT 6 ,
  DailyCal INTEGER UNSIGNED  NULL  ,
  WeekCal INTEGER UNSIGNED  NULL  ,
  MonthCal INTEGER UNSIGNED  NULL    ,
PRIMARY KEY(Calories))
TYPE=InnoDB;


CREATE TABLE Google API Restaurant (
  GeolocationAPIRestaurant INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT  ,
PRIMARY KEY(GeolocationAPIRestaurant))
TYPE=InnoDB;


CREATE TABLE Meals (
  MealID INTEGER UNSIGNED  NOT NULL DEFAULT 4 ,
  Calories INTEGER UNSIGNED  NOT NULL  ,
  Name VARCHAR  NULL DEFAULT 15 ,
  Proteins INTEGER UNSIGNED  NULL DEFAULT 3 ,
  Carbohydrates INTEGER UNSIGNED  NULL DEFAULT 3 ,
  Sugars INTEGER UNSIGNED  NULL DEFAULT 3 ,
  Fats INTEGER UNSIGNED  NULL DEFAULT 3 ,
  Barcore MEDIUMBLOB  NULL  ,
  TypeofMeal LINESTRING  NULL  ,
  Quantity INTEGER UNSIGNED  NULL DEFAULT 3   ,
PRIMARY KEY(MealID, Calories)  ,
INDEX Meals_FKIndex1(Calories),
  FOREIGN KEY(Calories)
    REFERENCES Calories(Calories)
      ON DELETE NO ACTION
      ON UPDATE CASCADE);


CREATE TABLE Restaurant (
  RestaurantID INTEGER UNSIGNED  NOT NULL DEFAULT 8  AUTO_INCREMENT,
  GeolocationAPIRestaurant INTEGER UNSIGNED  NOT NULL  ,
  MealID INTEGER UNSIGNED  NOT NULL  ,
  Calories INTEGER UNSIGNED  NOT NULL  ,
  Name INTEGER UNSIGNED  NULL DEFAULT 15 ,
  Category LINESTRING  NULL  ,
  Phone INTEGER UNSIGNED  NULL DEFAULT 10 ,
  Rating MEDIUMBLOB  NULL    ,
PRIMARY KEY(RestaurantID, GeolocationAPIRestaurant, MealID, Calories)  ,
INDEX Restaurant_FKIndex1(GeolocationAPIRestaurant)  ,
INDEX Restaurant_FKIndex2(MealID, Calories),
  FOREIGN KEY(GeolocationAPIRestaurant)
    REFERENCES Google API Restaurant(GeolocationAPIRestaurant)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,
  FOREIGN KEY(MealID, Calories)
    REFERENCES Meals(MealID, Calories)
      ON DELETE NO ACTION
      ON UPDATE CASCADE);


CREATE TABLE Profile (
  PlanID CHAR  NOT NULL DEFAULT 1 ,
  GeolocationAPI User INTEGER UNSIGNED  NOT NULL  ,
  MemberID INTEGER UNSIGNED ZEROFILL  NOT NULL  ,
  Phone INTEGER UNSIGNED  NOT NULL DEFAULT 10 ,
  Gender VARCHAR BINARY  NULL DEFAULT 2 ,
  Age  INTEGER UNSIGNED  NULL DEFAULT 3 ,
  PhotoID LONGBLOB  NULL    ,
PRIMARY KEY(PlanID, GeolocationAPI User, MemberID)  ,
INDEX Profile_FKIndex2(GeolocationAPI User)  ,
INDEX Profile_FKIndex6(MemberID),
  FOREIGN KEY(GeolocationAPI User)
    REFERENCES Google API User(GeolocationAPI User)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,
  FOREIGN KEY(MemberID)
    REFERENCES Users(MemberID)
      ON DELETE NO ACTION
      ON UPDATE CASCADE);


CREATE TABLE Profile_has_Calories (
  MemberID INTEGER UNSIGNED ZEROFILL  NOT NULL  ,
  GeolocationAPI User INTEGER UNSIGNED  NOT NULL  ,
  PlanID CHAR  NOT NULL  ,
  Calories INTEGER UNSIGNED  NOT NULL    ,
PRIMARY KEY(MemberID, GeolocationAPI User, PlanID, Calories)  ,
INDEX Profile_has_Calories_FKIndex1(PlanID, GeolocationAPI User, MemberID)  ,
INDEX Profile_has_Calories_FKIndex2(Calories),
  FOREIGN KEY(PlanID, GeolocationAPI User, MemberID)
    REFERENCES Profile(PlanID, GeolocationAPI User, MemberID)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,
  FOREIGN KEY(Calories)
    REFERENCES Calories(Calories)
      ON DELETE NO ACTION
      ON UPDATE CASCADE)
TYPE=InnoDB;


CREATE TABLE Orders (
  orderID INTEGER UNSIGNED  NOT NULL DEFAULT 5  AUTO_INCREMENT,
  MemberID INTEGER UNSIGNED ZEROFILL  NOT NULL  ,
  GeolocationAPIRestaurant INTEGER UNSIGNED  NOT NULL  ,
  RestaurantID INTEGER UNSIGNED  NOT NULL  ,
  GeolocationAPI User INTEGER UNSIGNED  NOT NULL  ,
  PlanID CHAR  NOT NULL  ,
  MealID INTEGER UNSIGNED  NOT NULL  ,
  Calories INTEGER UNSIGNED  NOT NULL  ,
  OrderCost DOUBLE  NULL DEFAULT (5,2)   ,
PRIMARY KEY(orderID, MemberID, GeolocationAPIRestaurant, RestaurantID, GeolocationAPI User, PlanID, MealID, Calories)  ,
INDEX Orders_FKIndex2(RestaurantID, GeolocationAPIRestaurant, MealID, Calories)  ,
INDEX Orders_FKIndex4(GeolocationAPI User, MemberID, PlanID),
  FOREIGN KEY(RestaurantID, GeolocationAPIRestaurant, MealID, Calories)
    REFERENCES Restaurant(RestaurantID, GeolocationAPIRestaurant, MealID, Calories)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,
  FOREIGN KEY(GeolocationAPI User, MemberID, PlanID)
    REFERENCES Profile(GeolocationAPI User, MemberID, PlanID)
      ON DELETE NO ACTION
      ON UPDATE CASCADE);



