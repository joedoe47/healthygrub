#/bin/sh
#Script run inside a new server (work in progress)




####
#settings

installer="apt-get" #switch for yum if needed 
originhost="nyc2.joepcs.com"



####
#Functions

function checkapp {
command -v "$1" >/dev/null 2>&1 || { echo >&2 "I will try to install: $1"; sleep 2; $installer install "$1"; }
}

function installzip {
wget -o setup.zip "$1" && unzip -d "/opt/$1" setup.zip
}

function installtar {
wget -o setup.tar "$2" && tar xfv -C "/opt/$1" setup.tar
}



####
#check requirements
apt update
checkapp python-software-properties
checkapp git



####
#Add app repositories
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
add-apt-repository 'deb http://mirror.jmu.edu/pub/mariadb/repo/5.5/debian jessie main'

#letsencrypt
git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt



####
#update app repositories
$installer update

#install 
$installer install rsync sed nginx mariadb-server mariadb-galera-server galera php5-fpm php5-mysqlnd git-annex 



####
#configure applications


#stop services
service nginx stop
service mariadb stop

#set up git user for data transfer
echo "setting up git user, give it any password, I will set password to 'git' and secure this account";
adduser git;
#we want this default password and will be disabled later
echo "git" | passwd git --stdin
usermod -G www-data git
mkdir -p /home/git/.ssh
ssh-keygen -t rsa -b 3096 -N "" -f /home/git/.ssh/id_rsa
chmod 700 /home/git/.ssh && chmod 600 /home/git/.ssh/*
cat /home/git/.ssh/id_rsa.pub | ssh git@"$originhost" 'cat - >> ~/.ssh/authorized_keys'


#letsencrypt https
echo "please enter the domain/url of this server"
read topdomain;
bash /opt/letsencrypt-auto certonly --standalone -d "$topdomain"

#git-annex get project
cd /home/root
git clone ssh://git@"$originhost":/home/git/config
cd config
git-annex get .
git-annex init "$HOSTNAME"

#www data
cd /var/www
git clone ssh://git@"$originhost":/home/git/html
cd html
git-annex get .
git-annex init "$HOSTNAME"
cd /var/www
sudo chown -hR git:www-data 


#galega









case "$1" in

install) ;;





*) ;;

esac














