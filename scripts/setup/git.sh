#!/bin/bash
### install centeral data server

#usage:
# - clone captsone project here
# - control other servers (if configured correct)
# - act as centeral MariaDB server

#run only as root
test "$(whoami)" != "root" && (echo 'run as root!'; exit 1)
#variables that you may want to change if needed
gitserver="git.joepcs.com"
webdir="/var/www"
aptinstaller="apt-get install"
rpminstaller="yum install"
#leave blank 
adminuser=" " 
installer=" "
searcher=" "
applist=" "
serverurl=" "

###     Functions    ###
######

#queue install if not installed
checkapp () { 
hash "$2" 2>/dev/null || { applist="$applist $2";  } 
}

#install function 
installapps () { 
$installer "$applist" 
}

#install misnamed apps (eg. maraiadb vs mairadb-server & mariadb-client) 
specialinstall () { 
if [ "$installer" = "$2" ]; then
$installer "$3"
else 
echo "skipping, we use $installer"
fi
}

#run commands that only work in deb or rpm systems
specialdo () { 
if [ "$installer" = "$2" ]; then
$3
else
echo "skipping, we use $installer"
fi
}

#add user for public gitweb access
#todo: use this to distribute key
addgitwebuser () {
echo "Adding user for gitweb (public https read access to repository)"
echo "username $i:"; read name
echo "password $i:"; read password
echo "$password" | htpasswd -c -i /home/git/.htpswd "$name";
}

#cleanup after root has setup everything for git user
cleanup () {
rm /home/root/.ssh/id_ecdsa /home/root/.ssh/id_ecdsa.pub
chown -hR git:git /home/git
sed -i "s/joepcs.com/${serverurl}/g" /etc/nginx/sites-enabled/default
}

#get config data from gitserver
getconfig () {
mkdir -p /home/git/capstoneconf || { exit 1; } 
cd /home/git/capstoneconf && git init && git-annex init "$HOSTNAME" && git remote add origin  git@"$gitserver":capstoneconf && git config remote.origin.annex-ignore false && git-annex sync && git-annex sync --content
}

#install cronjobs
#todo: use git hooks instead
installcron () {
crontab /home/git/config/schedule/"$2"/default.cron
}

#SSH key (user: git | password: [blank])
#todo: get from drive and prompt user/password to decrypt
getkey () {
cat <<EOT > /home/git/.ssh/id_ecdsa
-----BEGIN EC PRIVATE KEY-----
MIHcAgEBBEIBY02wiMFoHBOnHD7NRa7f8apm3VAo8tQVoTHngyQfPsdtimVsdUn3
nweZarRwKrBA/J3e+J6TnkMgsT+zqUalZ3agBwYFK4EEACOhgYkDgYYABAAJqCpm
4y0WhXb3PxV/UDdSGeFbnJB3Wy23Ss8/nCqpPsjgqv5KAg1eSdFQc8E2U0LeYsbe
bggUtC4Laz7+PnNkygB/jcgx02iFZ3bi2cJQtyG2RXYDqm6j0V5vFdQ4AqdRnSYV
AIcE2lrSpKS4Fcc//6/Zd7d51/a569rdEPfn4/6qTA==
-----END EC PRIVATE KEY-----
EOT

cat <<EOT > /home/git/id_ecdsa.pub
ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAAJqCpm4y0WhXb3PxV/UDdSGeFbnJB3Wy23Ss8/nCqpPsjgqv5KAg1eSdFQc8E2U0LeYsbebggUtC4Laz7+PnNkygB/jcgx02iFZ3bi2cJQtyG2RXYDqm6j0V5vFdQ4AqdRnSYVAIcE2lrSpKS4Fcc//6/Zd7d51/a569rdEPfn4/6qTA== git@cluster
EOT
}

###main logic

hash "apt-get" 2>/dev/null && { installer="apt-get install"; } 
hash "yum" 2>/dev/null && { installer="yum install"; }
#set up git user and ssh/keys
id -u git 2>/dev/null || adduser --system --shell /bin/bash --gecos 'git control' --group --disabled-password --home /home/git git && sudo usermod -a -G git www-data
#get ssh keys
getkey
cp /home/git/.ssh/id_ecdsa* /home/root/.ssh 
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
#query user 
echo "please enter the domain/url for your server (eg. nyc2.joepcs.com)" && read serverurl
#save server url for cron tasks 
echo "SERVERURL=$serverurl" >> /home/git/.profile
#get basic apps
checkapp . nginx
checkapp . git
checkapp . git-core
checkapp . git-annex
checkapp . apache2-utils
checkapp . gitweb
checkapp . fcgiwrap
checkapp . spawn-fcgi
checkapp . mariadb
installapps

getconfig

installcron . git

#customize for host
cp  /home/git/capstoneconf/settings/nginx/git /etc/nginx/sites-available/default

ln -s /home/git/capstoneconf/settings/nginx/nginx.conf /etc/nginx/nginx.conf
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

git clone https://github.com/sitaramc/gitolite.git /home/git/gitolite
mkdir -p bin
./gitolite/install -ln
echo 'export PATH=/home/git/bin:$PATH' >> /home/git/.profile
export PATH=/home/git/bin:$PATH
gitolite setup -pk /home/git/.ssh/id_ecdsa.pub
gitolite print-default-rc > /home/git/.gitolite.rc
sed -i -e "/'help',/a\\
'git-annex-shell ua'," /home/git/.gitolite.rc 
sed -i -e 's_/var/lib/git_/home/git/repositories_g' /etc/gitweb.conf
sed -i -e 's_$projectroot;_/home/git/projects.list_g' /etc/gitweb.conf

addgitwebuser


chmod g+r /home/git/projects.list
chmod -R g+rx /home/git/repositories

cd /home/git/gitolite-admin/ && git add . --all && git commit -m "first autocommit, will merge later" && git push --all

cleanup