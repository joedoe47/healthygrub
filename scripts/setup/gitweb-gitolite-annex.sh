#gitolite+gitweb+git-annex

#set up git user with *only* public key login
sudo adduser --system --shell /bin/bash --gecos 'git version control' --group --disabled-password --home /home/git git


#gitolite setup
sudo apt-get install git git-core git-annex apache2-utils
su git  
cd
git clone https://github.com/sitaramc/gitolite.git
mkdir -p bin
./gitolite/install -ln
echo 'export PATH=/home/git/bin:$PATH' >> .profile
export PATH=/home/git/bin:$PATH
#echo "paste your pubkey here and hit enter:"
#read pubkey
#echo "$pubkey" > admin.pub
gitolite setup -pk id_ecdsa.pub
gitolite print-default-rc > ~/.gitolite.rc
sed -e "/'help',/a\\
'git-annex-shell ua'," ~/.gitolite.rc > ~/.gitolite.rc


echo "how many users will we create to read this gitweb instance?"; read numbers
for i in `seq 1 $numbers`; do 
echo "username $i:"; read name
sudo htpasswd -c /home/git/.htpswd "$name"
done




#gitweb setup (optional)
sudo apt-get install highlight gitweb fcgiwrap spawn-fcgi
sed -i -e 's_/var/lib/git_/home/git/repositories_g' /etc/gitweb.conf
sed -i -e 's_$projectroot;_/home/git/projects.list_g' /etc/gitweb.conf
echo "$feature{'highlight'}{'default'} = [1];" >> /etc/gitweb.conf
usermod -a -G git www-data
chmod g+r /home/git/projects.list
chmod -R g+rx /home/git/repositories


cat <<EOT >> /etc/nginxsites-available/default
server {
listen 81;
root /usr/share/gitweb/;
location /index.cgi {
auth_basic "Restricted";
auth_basic_user_file /home/git/.htpswd;
include fastcgi_params;
gzip off;
fastcgi_param SCRIPT_NAME $uri;
fastcgi_param GITWEB_CONFIG /etc/gitweb.conf;
fastcgi_param SCRIPT_FILENAME  /usr/share/gitweb/gitweb.cgi;
fastcgi_pass  unix:/var/run/fcgiwrap.socket;
}
location / {
index index.cgi;
}
}
EOT
