$(document).ready(function() {

//
//only show content for logged in users (we may need all 3 entries for something)
//
var username = localStorage.getItem("name");
var email = localStorage.getItem("email");
var key = localStorage.getItem("key");

if (username === null || username === "" || username === "null" || username === undefined){ useronly(); }

if (email === null || email === "" || email === "null" || email === undefined){ useronly(); }

if (key === null || key === "" || key === "null" || key === undefined){
    useronly();
} else {  greeting(); }

function useronly() {
$("#check-auth").html("<p>You need to login to see this content</p>");
}

function greeting() {
$("#info2").html('Hello, <b>'+ localStorage.getItem("name") + '</b>');
//show buttons for logged in users
$("#register-btn").toggle(500);
$("#login-btn").toggle(500);
$("#logout-btn").toggle(500);
}

//
// Show/Hide Buttons
//

//toggle register form on click

$('#register-btn').on("click", function () {
//$("#register-btn").click(function(){
        $("#auth-user").hide(500);
        $("#new-user").toggle(500);
    });
    
//toggle login form on click
$("#login-btn").on("click", function () {
        $("#new-user").hide(500);
        $("#auth-user").toggle(500);
    });
    
//close register form on click
$("#register-btn-close").on("click", function () {
        $("#new-user").hide(500);
    });
    
//close login form on click
$("#login-btn-close").on("click", function () {
        $("#auth-user").hide(500);
    });

//
// Logout
//

$("#logout-btn").on("click", function () {
    logout();
    });

function logout() {
$("#register-btn").toggle(500);
$("#login-btn").toggle(500);
$("#logout-btn").toggle(500);
localStorage.removeItem("name");
localStorage.removeItem("email");
localStorage.removeItem("key");
$("#info2").html(''); //reset greeting
}
    
//
//    Register
//
    
/* validate register form */
	 $("#register-form").validate({
      rules:{
			name: {
		    required: true,
			minlength: 3 },
			password: {
			required: true,
			minlength: 8, },
			cpassword: {
			required: true,
			equalTo: '#rpassword' },
			email: {
            required: true,
            email: true}, },
      messages: {
            name: "please enter user name",
      password:{
            required: "please provide a password",
            minlength: "password at least have 8 characters" },
            email: "please enter a valid email address",
			cpassword:{
			required: "please retype your password",
			equalTo: "password doesn't match !"
			}
       },
	   submitHandler: submitForm1
       });
/* POST register form */
    function submitForm1() {
    var data = $("#register-form").not("#cpassword").serialize();

    $.ajax({
    type : 'POST',
    url  : 'https://nyc2.joepcs.com/api/v1/register',
    data : data,
    beforeSend: function()
    {
     $("#btn-submit1").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
	dataType: 'json',
    success :  function(data){
	console.log(data);
	if (data.error === false){
    $("#btn-submit1").html('<span class="glyphicon glyphicon-ok"></span> &nbsp;  All set!');
    $("#new-user").hide(1000);
	} else if (data.error === true){
	$("#btn-submit1").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');
	$("#error1").show(500).delay(10000).hide();
	//$("#error1").show(500);
	alert("Email address is already in use.");
//	$("#error-msg").html('<strong> Email address is already in use. </strong>');
	console.log(data.message)
	} else {
	alert("something is not right");
//	$("#error-msg").html('<strong> Something is not right </strong>');
	console.log(data.message)
	}
    }
    });
    return false;
  }

//
// Login
//
/* login form validation */
$("#login-form").validate({
    rules:{
	password: {
	required: true, },
	email: {
    required: true,
    email: true}, },
    messages: {
    password:{ required: "please provide a password"},
    email: "please enter a valid email address",
    },
	submitHandler: submitForm2
    });
/* POST login form */
    function submitForm2() {
    var data = $("#login-form").serialize();
    var url = "nyc2.joepcs.com";

    $.ajax({
    type : 'POST',
    url  : 'https://nyc2.joepcs.com/api/v1/login',
    data : data,
    beforeSend: function(){
     //$("#error2").fadeOut();
     $("#btn-submit2").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
    dataType: 'json',
    success :  function(data){
	console.log(data);
	if (data.error === false){
    //localsotrage
    if (typeof(Storage) !== "undefined") {
    $('#auth-user').toggle(400);
    localStorage.setItem("key", data.apiKey);
    localStorage.setItem("email", data.email);
    localStorage.setItem("name", data.name);
    } else {
    alert("webstorage not supported");
    }
    //welcome
    greeting();
	} else if (data.error === true){
	alert("username/password is incorrect");
	//$("#error-msg").html('<strong> username/password is incorrect </strong>');
	//$("#error1").show(500).delay(10000).hide();
	//$("#error1").show(500);
	} else {
	$("#btn-submit2").html('<span class="glyphicon glyphicon-log-in"></span>&nbsp;Login');
	$("#error-msg").html('<strong> Something is not right </strong>');
	console.log(data.message)
	}
    }
    });
    return false;
  }

//
// Change password
//
   /* change password form submit */
	$("#login-reset-form").validate({
    rules:{
			password:{ required: true,
			minlength: 8, },
			cpassword: { required: true,
			equalTo: '#password', }, },
    messages:{
    password:{
            required: "please provide a password",
            minlength: "password at least have 8 characters" },
			cpassword:{
			required: "please retype your password",
			equalTo: "password doesn't match !"
	}
    },
	   submitHandler: submitForm3
       });
		 /* form submit */
    function submitForm3() {
    var data = $("#login-form").serialize();

    $.ajax({
    type : 'put',
    url  : 'https://nyc2.joepcs.com/api/v1/reset',
    data : data,
    beforeSend: function(){
    xhr.setRequestHeader ("Authorization:" + localStorage.getItem("key") );
    $("#btn-submit2").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
	dataType: 'json',
    success :  function(data){
	console.log(data);
	if (data.error === false){
    $("#info3").html('<strong>'+ data.message +'</strong>');
	} else {
	$("#info3").html('<strong> Something is not right </strong>');
	console.log(data.message)
	}
    }
    });
    return false;
  }


//end
  });
