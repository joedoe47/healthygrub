$(document).ready(function() {

//show register form
$("#register-btn").click(function(){
        $("#register-form").toggle(500);
    });
    
/* validate register form */
	 $("#register-form").validate({
      rules:{
			name: {
		        required: true,
			      minlength: 3 },
			password: {
			      required: true,
			      minlength: 8, },
			cpassword: {
			      required: true,
			      equalTo: '#password' },
			email: {
            required: true,
            email: true}, },
      messages: {
            name: "please enter user name",
      password:{
            required: "please provide a password",
            minlength: "password at least have 8 characters" },
            email: "please enter a valid email address",
			cpassword:{
						required: "please retype your password",
						equalTo: "password doesn't match !"
					  }
       },
	   submitHandler: submitForm1
       });
/* POST register form */
    function submitForm1() {
    var data = $("#register-form").not("#cpassword").serialize();

    $.ajax({

    type : 'POST',
    url  : 'https://nyc2.joepcs.com/api/v1/register',
    data : data,
    beforeSend: function()
    {
     //$("#error1").fadeOut();
     $("#btn-submit1").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
		dataType: 'json',
    success :  function(data){
					 console.log(data);
					 if (data.error === false){
            $("#btn-submit1").html('<span class="glyphicon glyphicon-ok"></span> &nbsp;  All set!');
					 }
					 else if (data.error === true){
					   $("#btn-submit1").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');
					   $("#info1").html('<strong> Email address is already in use. </strong>');
						 console.log(data.message)
					 }
					 else {
					   $("#info1").html('<strong> Something is not right </strong>');
						 console.log(data.message)
					 }
         }
    });
    return false;
  }


/* login form validation */
	 $("#login-form").validate({
      rules:{
			password: {
			      required: true, },
			email: {
            required: true,
            email: true}, },
      messages: {
      password:{ required: "please provide a password"},
      email: "please enter a valid email address",
       },
	   submitHandler: submitForm2
       });
/* POST login form */
    function submitForm2() {
    var data = $("#login-form").serialize();

    $.ajax({

    type : 'POST',
    url  : 'https://nyc2.joepcs.com/api/v1/login',
    data : data,
    beforeSend: function()
    {
     //$("#error2").fadeOut();
     $("#btn-submit2").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
		dataType: 'json',
    success :  function(data){
					 console.log(data);
					 if (data.error === false){
					  $('#login-form').toggle(400);
           
           Cookies.set("key", data.apiKey, {
            expires : 7,  //days
            path    : '/', domain  : 'nyc2.joepcs.com',
            secure  : true  });
             
           Cookies.set("email", data.email, {
            expires : 7,  //days
            path    : '/', domain  : 'nyc2.joepcs.com',
            secure  : true  });
            
           Cookies.set("name", data.name, {
            expires : 7,  //days
            path    : '/', domain  : 'nyc2.joepcs.com',
            secure  : true  });
            
$("#info2").html('Hi, <b>'+ Cookies.get('name') + '</b>');
alert('<strong> Hi, '+ Cookies.get('name') + '</strong>');
					 }
					 else {
					   $("#info2").html('<strong> Something is not right </strong>');
						 console.log(data.message)
					 }
         }
    });
    return false;
  }
  
   /* change password form submit */
	 $("#login-reset-form").validate({
      rules:{
			password: { required: true,
			minlength: 8, },
			cpassword: { required: true,
			  equalTo: '#password', }, },
      messages: {
      password:{
            required: "please provide a password",
            minlength: "password at least have 8 characters" },
			cpassword:{
						required: "please retype your password",
						equalTo: "password doesn't match !"
					  }
       },
	   submitHandler: submitForm3
       });
		 /* form submit */
    function submitForm3() {
    var data = $("#login-form").serialize();

    $.ajax({

    type : 'put',
    url  : 'https://nyc2.joepcs.com/api/v1/reset',
    data : data,
    beforeSend: function()
    {
      xhr.setRequestHeader ("Authorization:" + Cookies.get('key') );
     //$("#error3").fadeOut();
     $("#btn-submit2").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
		dataType: 'json',
    success :  function(data){
					 console.log(data);
					 if (data.error === false){
            $("#info3").html('<strong>'+ data.message +'</strong>');


					 }
					 else {
					   $("#info3").html('<strong> Something is not right </strong>');
						 console.log(data.message)
					 }
         }
    });
    return false;
  }
  });
