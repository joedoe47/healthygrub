
<div id="check-auth">

<p>please complete your profile


<!-- Profile -->
<form class="form-horizontal"  id="profile">
<fieldset>
<legend>Profile</legend>

<!-- Appended Input-->
<div class="control-group">
  <label class="control-label" for="height">Height</label>
  <div class="controls">
    <div class="input-append">
      <input id="height" name="height" class="input-medium" placeholder="5" type="text" required="">
      <span class="add-on">Ft.</span>
    </div>
    <p class="help-block">round up to the highest whole number for your height</p>
  </div>
</div>

<!-- Appended Input-->
<div class="control-group">
  <label class="control-label" for="weight">Weight</label>
  <div class="controls">
    <div class="input-append">
      <input id="weight" name="weight" class="input-medium" placeholder="190" type="text" required="">
      <span class="add-on">Lbs</span>
    </div>
    <p class="help-block">number only input</p>
  </div>
</div>

<!-- Select Basic -->
<div class="control-group">
  <label class="control-label" for="gender">Gender</label>
  <div class="controls">
    <select id="gender" name="gender" class="input-medium">
      <option>Male</option>
      <option>Female</option>
    </select>
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label" for="submit"></label>
  <div class="controls">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>

</p>
<!-- load later -->
<script>
$(document).ready(function() {

//
// Change profile
//
   /* change password form submit */
	$("#profile").validate({
    rules:{
	height:{ required: true, },
	weight:{ required: true, },
	gender:{ required: true, },
    messages:{
    height:{ required: "please provide a password" },
    weight:{ required: "please provide a password" },
    gender:{ required: "please provide a password" },
    },
	submitHandler: submitForm4
    });
		 /* form submit */
    function submitForm4() {
    var data = $("#profile").serialize();

    $.ajax({
    type : 'post',
    url  : 'https://nyc2.joepcs.com/api/v1/profile',
    data : data,
    beforeSend: function(){
    xhr.setRequestHeader ("Authorization:" + localStorage.getItem("key") );
    $("#btn-submit2").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
    },
    dataType: 'json',
    success :   function(data){
	console.log(data);
	if (data.error === false){
    $("#info3").html('<strong>'+ data.message +'</strong>');
    $("#btn-submit1").html('<span class="glyphicon glyphicon-ok"></span> &nbsp;  d');
	} else {
	$("#info3").html('<strong> Something is not right </strong>');
	console.log(data.message)
	}
    }
    });
    return false;
  }

//end
});
</script>

</div>