<div id="check-auth">

<h1>Order food</h1>


<button onclick="getLocation()">Try It</button>

<p id="demo">Click the button to get your position.</p>

<div id="mapholder"></div>

<div id="json"> </div>

<script>
var x = document.getElementById("demo");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
        navigator.geolocation.getCurrentPosition(showPosition2);
        navigator.geolocation.getCurrentPosition(search);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition2(position) {
    //x.innerHTML = "Latitude: " + position.coords.latitude +
    //"<br>Longitude: " + position.coords.longitude;
    $("#demo").html(localStorage.getItem("lata") + "<br>" + localStorage.getItem("long"));
    
    //save cordinates
    localStorage.setItem("lata",  position.coords.longitude);
    localStorage.setItem("long", position.coords.latitude);
}

function showPosition(position) {
    var latlon = position.coords.latitude + "," + position.coords.longitude;

    var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="
    +latlon+"&zoom=14&size=400x300&sensor=true";
    document.getElementById("mapholder").innerHTML = "<img src='"+img_url+"'>";
}

function search(position) {
    //var data = $("#login-form").serialize();
    //var data = {lat: localStorage.getItem("lata"), long: localStorage.getItem("long")};
    
    $.ajax({
    type : 'POST',
    url  : 'https://nyc2.joepcs.com/api/v2/allmeals',
    data :  {lat: localStorage.getItem("lata"), long: localStorage.getItem("long")},
    //crossDomain: true,
    beforeSend: function(xhr){
    xhr.setRequestHeader('Authorization: ', localStorage.getItem("key"))
    },
    dataType: 'json',
    success :  function(data){
	console.log(data);
	if (data.error === false){
    //localsotrage
    if (typeof(Storage) !== "undefined") {
console.log(data.message)
    $("#json").html(data);

    } else {
    alert("webstorage not supported");
    }
    //what if no websotrage D:

	} else if (data.error === true){
	//problem

	} else {
    //other problem
    
	console.log(data.message)
	}
    }
    });
    
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}


</script>

</div>